#include <avr/io.h>
#include <avr/interrupt.h>
#define F_CPU 8000000UL
#include <util/delay.h>

//A0 - dir1
//A1 - dir2
//A2 - I1
//A3 - I2
//A4 - En1
//A5 - En2

#define ENA1A_ENA (PORTA|= 0b00010000)
#define ENA1A_DIS (PORTA&=~0b00010000)

#define ENA1B_ENA (PORTA|= 0b00100000)
#define ENA1B_DIS (PORTA&=~0b00100000)

#define DIR1A_1   (PORTA|= 0b00000001)
#define DIR1A_0   (PORTA&=~0b00000001)

#define DIR1B_1   (PORTA|= 0b00000010)
#define DIR1B_0   (PORTA&=~0b00000010)

#define I1A_0 (PORTA|= 0b00000100)
#define I1A_2 (PORTA&=~0b00000100)

#define I1B_0 (PORTA|= 0b00001000)
#define I1B_2 (PORTA&=~0b00001000)

/////////////////////////////////////////
#define ENA2A_ENA (PORTB|= 0b00010000)
#define ENA2A_DIS (PORTB&=~0b00010000)

#define ENA2B_ENA (PORTB|= 0b00100000)
#define ENA2B_DIS (PORTB&=~0b00100000)

#define DIR2A_1   (PORTB|= 0b00000001)
#define DIR2A_0   (PORTB&=~0b00000001)

#define DIR2B_1   (PORTB|= 0b00000010)
#define DIR2B_0   (PORTB&=~0b00000010)

#define I2A_0 (PORTB|= 0b00000100)
#define I2A_2 (PORTB&=~0b00000100)

#define I2B_0 (PORTB|= 0b00001000)
#define I2B_2 (PORTB&=~0b00001000)

#define ACCELERATION_STEPS 500

void TransmitByte (unsigned char data);
inline void ReceiveByte (void);
void InitUART (unsigned char baudrate);
unsigned char komenda,uart_flaga=0;

#define RECEIVE do{while(uart_flaga==0){ReceiveByte();}uart_flaga=0;}while(0)

inline void journey();
inline void get_command();
inline int get_timeconst();
inline void move1();
inline void move2();

unsigned int steps=0;
char dir1=0,dir2=0;
char state1,state2;
char en1=1,en2=1;

#define ACC 1
#define DEC 2
#define CON 3
volatile char move_section;

#define ACCELERATION_STEPS 250
 //magic const

#define RAMP    250 //every this period (of timer) speed (dt value) will be changed

int init_dt=13000;
unsigned int min_dt=4000;

volatile unsigned int dt;
unsigned int steps_acceleration;//valid only when engines moves
unsigned int steps_deceleration;
unsigned int steps_constant;

int main(){
	DDRA=0xFF;
	DDRB=0xFF;
	DDRC=0xFF;
	DDRD=0;

	ENA1A_DIS;
	ENA1B_DIS;
	ENA2A_DIS;
	ENA2B_DIS;

	InitUART(52);//9600bps@8MHz

	//TransmitByte(0xCB);
	uart_flaga=0;

	while(1){
		ReceiveByte();
		if(uart_flaga==1){
			uart_flaga=0;
			if(komenda=='S'){
				TransmitByte('R');
				get_command();
				_delay_ms(10);
				TransmitByte('S');
				if(steps!=0)journey();
				TransmitByte('E');
			}
			if(komenda=='I'){//init
				TransmitByte('R');
				init_dt=get_timeconst();
				TransmitByte('E');
			}
			if(komenda=='M'){//min dt (max speed)
				TransmitByte('R');
				min_dt=get_timeconst();
				TransmitByte('E');
			}
		}
	}
}

inline int get_timeconst(){
    int res;
	RECEIVE;
	res=komenda<<8;
	RECEIVE;
	res|=komenda;

	///UWAGA: ZMIENIŁEM COŚTAM
    return res;
}

inline void get_command(){
	unsigned int icmd;
	//_delay_ms(10);
	//TransmitByte(0xFF);
	RECEIVE;
	icmd=komenda<<8;
	//TransmitByte(komenda);
	RECEIVE;
	icmd|=komenda;
	//TransmitByte(komenda);

    /*command=0b EDednnnn.nnnnnnnn (MSB.LSB)
        where:
        E-1st engine enable
        e-2nd engine enable
        D-1st engine direction
        d-2nd engine direction
        n(x12)- number of steps
    */

	char hlp;
	hlp=icmd>>12;

	steps=icmd&0xFFF;

	en1=0;
	en2=0;
	dir1=0;
	dir2=0;

	if(hlp&1)dir2=1;
	hlp>>=1;

	if(hlp&1)en2=1;
	hlp>>=1;

	if(hlp&1)dir1=1;
	hlp>>=1;

	if(hlp&1)en1=1;
	hlp>>=1;
}

ISR(TIMER1_OVF_vect){
    TCNT1=~RAMP;
    if(move_section==ACC){
        if(dt>min_dt)dt-=1;//acceleration
    }else if(move_section==DEC){
        if(dt<init_dt)dt+=1;//deceleration
    }
}

inline void journey(){//FIX: at 0 doesn't work
	ENA1A_ENA;
	ENA1B_ENA;
	ENA2A_ENA;
	ENA2B_ENA;

    if(ACCELERATION_STEPS*2>=steps){
        steps_acceleration=steps/2;
        steps_deceleration=steps-steps_acceleration;
        steps_constant=0;
    }else{
        steps_acceleration=ACCELERATION_STEPS;
        steps_deceleration=ACCELERATION_STEPS;
        steps_constant=steps-2*ACCELERATION_STEPS;
    }

	dt=init_dt;

	_delay_ms(400);

    TCNT1=~RAMP;
    TIMSK|=(1 << TOIE1); // Enable overflow interrupt
    TCCR1B |= (1 << CS11); // Set up timer at Fcpu/8

    sei();//global interrupts eneble

    move_section=ACC;
    while(steps_acceleration!=0){
        _delay_us(dt);
		if(en1)move1();
		if(en2)move2();
		steps_acceleration--;
    }

    move_section=CON;
    while(steps_constant!=0){
        _delay_us(dt);
		if(en1)move1();
		if(en2)move2();
		steps_constant--;
    }

    move_section=DEC;
    while(steps_deceleration!=0){
        _delay_us(dt);
		if(en1)move1();
		if(en2)move2();
		steps_deceleration--;
    }

    cli();//global interrupts disable

	_delay_ms(400);
	ENA1A_DIS;
	ENA1B_DIS;
    ENA2A_DIS;
	ENA2B_DIS;
}

inline void move1(){
	state1&=3;
	if(state1==0){
		DIR1A_0;
		I1A_0;
		DIR1B_0;
		I1B_2;
	}else if(state1==1){
		DIR1A_1;
		I1A_2;
		DIR1B_0;
		I1B_0;
	}else if(state1==2){
		DIR1A_1;
		I1A_0;
		DIR1B_1;
		I1B_2;
	}else if(state1==3){
		DIR1A_0;
		I1A_2;
		DIR1B_1;
		I1B_0;
	}
	if(dir1)state1+=1;
	else state1-=1;
	state1&=3;
}

inline void move2(){
	state2&=3;
	if(state2==0){
		DIR2A_0;
		I2A_0;
		DIR2B_0;
		I2B_2;
	}else if(state2==1){
		DIR2A_1;
		I2A_2;
		DIR2B_0;
		I2B_0;
	}else if(state2==2){
		DIR2A_1;
		I2A_0;
		DIR2B_1;
		I2B_2;
	}else if(state2==3){
		DIR2A_0;
		I2A_2;
		DIR2B_1;
		I2B_0;
	}
	if(dir2)state2+=1;
	else state2-=1;
	state2&=3;
}

/* Initialize UART */
void InitUART (unsigned char baudrate){
  /* Set the baud rate */
  UBRRL = baudrate;
  UBRRH = 0;

  /* Enable UART receiver and transmitter */
  UCSRB = (1 << RXEN) | (1 << TXEN);

  /* set to 8 data bits, 2 stop bit */
  UCSRC = (1 << UCSZ1)|(3 << UCSZ0)|(1<<URSEL)|(1<<USBS);

}

/* Read and write functions */
inline void ReceiveByte (void){
	uart_flaga=0;
 	/* Check incomming data */
 	if (!(UCSRA & (1 << RXC)))return;

  	/* Return the data */
	uart_flaga=1;
	komenda=UDR;
	//_delay_ms(10);
	//TransmitByte(komenda);
 	return;
}

void TransmitByte (unsigned char data){
  /* Wait for empty transmit buffer */
  while (!(UCSRA & (1 << UDRE)));
  /* Start transmittion */
  UDR = data;
}
